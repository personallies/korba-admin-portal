<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/surfline/data', 'HomeController@getSurflineData')->name('home.surfline.data');
Route::get('/home/busy/data', 'HomeController@getBusyData')->name('home.busy.data');

Route::get('/zontal',function(){
    return view('zontal.index');
});