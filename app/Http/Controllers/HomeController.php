<?php

namespace App\Http\Controllers;

use App\BusyTransactionLogs;
use App\SurflineTransactionLogs;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Traits\ConsumeAPI;

class HomeController extends Controller
{
    use ConsumeAPI;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //call api
        $api_url = config('services.endpoint.surflineBalanceQuery');
        $result = $this->getRestMessage($api_url);

//        dd($result);
        $floatNum = floatval($result->balance)/100;
        $balance = number_format($floatNum,2,'.',',');

        return view('home',compact('balance'));
    }

    public function getSurflineData()
    {

        return Datatables::of(SurflineTransactionLogs::query())->make(true);
    }

    public function getBusyData()
    {

        return Datatables::of(BusyTransactionLogs::query())->make(true);
    }
}
