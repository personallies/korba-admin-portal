<?php
/**
 * Created by PhpStorm.
 * User: niiforte
 * Date: 11/28/17
 * Time: 3:19 PM
 */

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

trait ConsumeAPI
{
    /**
     * @param endpoint
     *
     * @return responseBody
     */
    public function getRestMessage($endpoint)
    {

        $client = new Client();

        try {
            $response = $client->get($endpoint);
            $responseBody = json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            $response = $this->StatusCodeHandling($e);
            return $response;
        }

        return $responseBody;
    }

    /**
     * @param endpoint
     * @param requestBody
     *
     * @return responseBody
     */
    public function postRestMessage($endpoint, $requestBody)
    {

        $client = new Client();

        try {
            $response = $this->$client->post($endpoint, ['query' => $requestBody]);
            $responseBody = json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            $response = $this->StatusCodeHandling($e);
            return $response;
        }

        return $responseBody;
    }

    /**
     * @param $e
     * @return mixed
     */
    public function StatusCodeHandling($e)
    {
        if ($e->getResponse()->getStatusCode() == ‘400’) {
            $this->prepare_access_token();
        } elseif ($e->getResponse()->getStatusCode() == ‘422’) {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        } elseif ($e->getResponse()->getStatusCode() == ‘500’) {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        } elseif ($e->getResponse()->getStatusCode() == ‘401’) {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        } elseif ($e->getResponse()->getStatusCode() == ‘403’) {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        } else {
            $response = json_decode($e->getResponse()->getBody(true)->getContents());
            return $response;
        }
    }


}