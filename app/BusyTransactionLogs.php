<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusyTransactionLogs extends Model
{
    //
    protected $connection = 'mysql1';

    protected $table = 'transaction_logs';
}
