<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurflineTransactionLogs extends Model
{
    //
    protected $connection = 'mysql2';

    protected $table = 'transaction_logs';
}
