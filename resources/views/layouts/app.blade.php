<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Korba Transflow | Welcome') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">

    {{--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/datatables/dataTables.foundation.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/datatables/dataTables.jqueryui.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/datatables/dataTables.semanticui.min.css') }}" rel="stylesheet">--}}
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" >
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Korba Transflow') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>

                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>

    {{--<script src="{{ asset('js/bootstrap.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/datatables/dataTables.foundation.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/datatables/dataTables.jqueryui.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/datatables/dataTables.semanticui.min.js') }}"></script>--}}
    @stack('additional_scripts')
</body>
</html>
