@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        {{--<div class="row">--}}
            <nav class="col-md-2 bg-light sidebar">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                     aria-orientation="vertical">

                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation" class="active">
                            <a href="#surfline" role="tab" data-toggle="tab" class="active">
                                <i class="octicon octicon-dashboard"></i> Surfline
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#busy" role="tab" data-toggle="tab">
                                <i class="octicon octicon-person"></i> Busy
                            </a>
                        </li>
                        {{--<li role="presentation">--}}
                        {{--<a href="#password" role="tab" data-toggle="tab">--}}
                        {{--<i class="octicon octicon-lock"></i> {{trans('labels.frontend.general.consumer.change_password')}}--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li role="presentation">--}}
                        {{--<a href="#wallet" role="tab" data-toggle="tab">--}}
                        {{--<i class="octicon octicon-credit-card"></i> {{trans('labels.frontend.general.consumer.wallet')}}--}}
                        {{--</a>--}}
                        {{--</li>--}}
                    </ul>
                </div>
            </nav>

            <main class="col-md-10" role="main">
                {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Dashboard</div>--}}
                <div class="tab-content">
                    <div class="tab-pane active" id="surfline">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="surfline-dt">
                                    <caption>
                                        Balance : GHS {{$balance}}
                                    </caption>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        {{--<th>Account Type</th>--}}
                                        <th>Amount</th>
                                        {{--<th>Bundle ID</th>--}}
                                        {{--<th>Error</th>--}}
                                        <th>Korba Transaction ID</th>
                                        <th>Msisdn</th>
                                        <th>Response Code</th>
                                        <th>Response Message</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                    </thead>

                                    @push('additional_scripts')
                                        <script>
                                            $(function () {
                                                $('#surfline-dt').DataTable({
                                                    processing: true,
                                                    serverSide: true,
                                                    ajax: '{!! route('home.surfline.data') !!}',
                                                    columns: [
                                                        {data: 'id', name: 'id'},
//                                                        {data: 'account_type', name: 'account_type'},
                                                        {data: 'amount', name: 'amount'},
//                                                        {data: 'bundle_id', name: 'bundle_id'},
//                                                        {data: 'error', name: 'error'},
                                                        {data: 'korba_trans_id', name: 'korba_trans_id'},
                                                        {data: 'msisdn', name: 'msisdn'},
                                                        {data: 'response_code', name: 'response_code'},
                                                        {data: 'response_message', name: 'response_message'},
                                                        {data: 'status', name: 'status'},
                                                        {data: 'created', name: 'created'},
                                                        {data: 'updated', name: 'updated'}
                                                    ],
                                                    "pagingType": "full_numbers",
                                                    "paging": "true"
                                                });
                                            });
                                        </script>
                                    @endpush
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="busy">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="busy-dt">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Amount</th>
                                        <th>Busy Transaction ID</th>
                                        <th>Error Code</th>
                                        <th>Korba Transaction ID</th>
                                        <th>Msisdn</th>
                                        <th>Price Plan Code</th>
                                        <th>Response Code</th>
                                        <th>Response Message</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                    </tr>
                                    </thead>

                                    @push('additional_scripts')
                                        <script>
                                            $(function () {
                                                $('#busy-dt').DataTable({
                                                    processing: true,
                                                    serverSide: true,
                                                    ajax: '{!! route('home.busy.data') !!}',
                                                    columns: [
                                                        {data: 'id', name: 'id'},
                                                        {data: 'amount', name: 'amount'},
                                                        {data: 'busy_trans_id', name: 'busy_trans_id'},
                                                        {data: 'error_code', name: 'error_code'},
                                                        {data: 'korba_trans_id', name: 'korba_trans_id'},
                                                        {data: 'msisdn', name: 'msisdn'},
                                                        {data: 'price_plan_code', name: 'price_plan_code'},
                                                        {data: 'response_code', name: 'response_code'},
                                                        {data: 'response_message', name: 'response_message'},
                                                        {data: 'status', name: 'status'},
                                                        {data: 'created', name: 'created'},
                                                        {data: 'updated', name: 'updated'}
                                                    ],
                                                    paging:true,
                                                    pagingType:'full_numbers'
//                                                    "pagingType": "full_numbers",
//                                                    "paging": "true"
                                                });
                                            });
                                        </script>
                                    @endpush
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</div>--}}
                {{--</div>--}}
            </main>
        {{--</div>--}}
    </div>
@endsection
